# CODE REDEMPTION APP BACKEND

## Description

Final Project Assignment for 'Telerik Academy Alpha' with JavaScript: Design and implement a web application that will allow users to scan codes, register redemption attempts and allow admins to mange them and view reports.

### Stack

Front-end:
- SPA - Angular
Git repository: https://gitlab.com/kriss99/coca-cola-frontend

Back-end:
- Database - MySQL
- REST API - NestJS
Git repository: https://gitlab.com/kriss99/coca-cola-backend

----------

# Getting started

## Installation

Run
    `git clone https://gitlab.com/kriss99/coca-cola-backend`

Get into the project's folder
    `cd forum-backend/backend-app`

## In order to run the project
1. run `npm install` to install all dependencies
2. run `npm start` to start the application
*Make sure that you have the database running too.

## Database

### MariaDB/MySQL

The example codebase uses [Typeorm](http://typeorm.io/) with a MariaDB database.

Create a new MariaDB database with the name `rootdb` (or the name specified in the ormconfig.json).

MariaDb database settings are in `ormconfig.json`.

Start local MariaDB server and create new database.

You will need to add an `.env` file which should include PORT, JWT_SECRET, DB_TYPE, DB_HOST, DB_PORT, DB_DATABASE_NAME, EMAIL and PASS.

Then you need to run the migration which will build the Database tables:

    npm run typeorm -- migration:run

You will also need to import the Sample Data by running `npm run seed`.

## Scripts

- `npm start` - Start application
- `npm run start:dev` - Start application in nodemon
- `npm run typeorm` - run Typeorm commands
- `npm run seed` - initial seed for the database

----------

# Authors

Kriss Svilenov

Radoslav Donchev 