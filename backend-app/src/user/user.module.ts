import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from '../core/services/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedemptionCode } from '../entity/RedemptionCode';
import { AuthModule } from '../auth/auth.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { RedemptionRecord } from '../entity/RedemptionRecord';
import { Outlet } from '../entity/Outlet';
import { CoreModule } from '../core/core.module';

@Module({
  imports: [ CoreModule, TypeOrmModule.forFeature([RedemptionCode, RedemptionRecord, Outlet]), PassportModule.register({ defaultStrategy: 'jwt' }),
  JwtModule.registerAsync({
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: async (configService: ConfigService) => ({
      secretOrPrivateKey: configService.jwtSecret,
      signOptions: {
        expiresIn: configService.jwtExpireTime,
      },
    }),
  })],
  controllers: [UserController],
  providers: [],
  exports: [],
})
export class UserModule { }
